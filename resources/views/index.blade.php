<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Photographic Evidence API</title>
</head>

<body>
    <h1>Photographic Evidence API - Gendra</h1>
    <p>The full API documentation can be found at
        <a href="https://documenter.getpostman.com/view/984767/TWDamagG" target="_blank"
            rel="noopener noreferrer">Photographic Evidence API Postman</a>
    </p>
    <p>
        Developed by <span style="font-weight: bold">Carlos Garcia</span>
    </p>
</body>

</html>
