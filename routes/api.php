<?php

use App\Http\Controllers\EvidenceController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

Route::prefix('v1')->middleware(['localization'])->group(function () {
    Route::post('/auth/token', [UserController::class, 'postAuthToken']);
    Route::get('/evidences', [EvidenceController::class, 'getEvidences']);
    Route::middleware(['auth:sanctum'])->group(function () {
        Route::get('/auth/me', [UserController::class, 'getAuthMe']);
        Route::middleware(['isAdmin'])->group(function () {
            Route::prefix('users')->group(function () {
                Route::get('/', [UserController::class, 'getUsers']);
                Route::get('/{user}', [UserController::class, 'getUserById']);
                Route::put('/{user}', [UserController::class, 'putUserById']);
                Route::delete('/{user}', [UserController::class, 'deleteUserById']);
                Route::post('/', [UserController::class, 'postCreateUser']);
            });
        });
        Route::prefix('evidences')->group(function () {
            Route::post('/', [EvidenceController::class, 'postCreateEvidence']);
            Route::post('/update/{evidence}', [EvidenceController::class, 'postUpdateEvidence']);
            Route::get('/my-evidences', [EvidenceController::class, 'getMyEvidences']);
            Route::get('/{evidence}', [EvidenceController::class, 'getEvidenceById']);
            Route::delete('/{evidence}', [EvidenceController::class, 'deleteEvidenceById']);
        });
    });
});
