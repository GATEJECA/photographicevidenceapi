<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateteEvidenceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "evidenced" => "date_format:Y-m-d",
            "description" => "min:2|max:500",
            "categoryid" => "integer|exists:categories,id",
            "category" => "string|min:2|max:100",
            "image" => "file|mimes:jpg,bmp,png"
        ];
    }
}
