<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateEvidenceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "evidenced" => "required|date_format:Y-m-d",
            "description" => "required|min:2|max:500",
            "categoryid" => "required_without:category|integer|exists:categories,id",
            "category" => "required_without:categoryid|string|min:2|max:100",
            "image" => "required|file|mimes:jpg,bmp,png"
        ];
    }
}
