<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|min:2|max:200',
            'nickname' => 'string|min:2|max:100|unique:users,nickname,' . $this->user->id,
            'type' => 'in:admin,user',
            'password' => 'string|min:2|max:200',
        ];
    }
}
