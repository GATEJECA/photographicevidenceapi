<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthTokenRequest;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function postAuthToken(AuthTokenRequest $request)
    {
        $user = User::nickname($request["user"])->firstOrFail();
        if (Hash::check($request["password"], $user->password)) {
            $token = $user->createToken("commonToken");
            return $this->createJsonGenericResponse(["token" => $token->plainTextToken]);
        }
        abort(422, __("invalidCredentials"));
    }
    public function getAuthMe()
    {
        return $this->createJsonGenericResponse(["user" => auth()->user()]);
    }
    public function getUsers()
    {
        return $this->createJsonGenericResponse(["users" => User::all()]);
    }
    public function getUserById(User $user)
    {
        return $this->createJsonGenericResponse(["user" => $user]);
    }
    public function postCreateUser(CreateUserRequest $request)
    {
        $data = array_merge(
            $request->only(['name', 'nickname', 'type']),
            ["password" => Hash::make($request["password"])]
        );
        $user = User::create($data);
        return $this->createJsonGenericResponse(["user" => $user]);
    }
    public function deleteUserById(User $user)
    {
        $user->delete();
        return $this->createJsonGenericResponse([], 204);
    }

    public function putUserById(UpdateUserRequest $request, User $user)
    {
        $data = $request->only(['name', 'nickname', 'type']);
        if ($request->password) {
            $data["password"] = Hash::make($request["password"]);
        }
        $user->update($data);
        return $this->createJsonGenericResponse(["user" => $user]);
    }
}
