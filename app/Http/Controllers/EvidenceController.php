<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEvidenceRequest;
use App\Http\Requests\UpdateteEvidenceRequest;
use App\Models\Category;
use App\Models\Evidence;
use Illuminate\Support\Facades\Storage;

class EvidenceController extends Controller
{
    public function postCreateEvidence(CreateEvidenceRequest $request)
    {
        $category_id = $request["categoryid"];
        if (!$category_id) {
            $category = Category::create(["category" => $request["category"]]);
            $category_id = $category['id'];
        }
        $data = array_merge(
            $request->only(['description']),
            ["category_id" => $category_id, "user_id" => auth()->user()["id"], 'evidence_date' => $request["evidenced"]]
        );
        $evidence = Evidence::create($data);
        $evidence->addMediaFromRequest('image')
            ->withResponsiveImages()
            ->toMediaCollection("Evidences", 's3');
        return $this->createJsonGenericResponse(["evidence" => $evidence]);
    }
    public function getEvidences()
    {
        $evidences = Evidence::with(["category", "user"])
            ->get()
            ->map(function ($item) {
                $mediaPath =  pathinfo($item->getFirstMediaPath("Evidences"), PATHINFO_DIRNAME);
                $this->getMediaUrl($item, $mediaPath);
                return $item;
            });
        return $this->createJsonGenericResponse(["evidences" => $evidences]);
    }
    public function getEvidenceById(Evidence $evidence)
    {
        $mediaPath =  pathinfo($evidence->getFirstMediaPath("Evidences"), PATHINFO_DIRNAME);
        $this->getMediaUrl($evidence, $mediaPath);
        return $this->createJsonGenericResponse(["evidence" => $evidence]);
    }
    public function deleteEvidenceById(Evidence $evidence)
    {
        $evidence->delete();
        return $this->createJsonGenericResponse([], 204);
    }

    public function getMyEvidences()
    {
        $evidences = Evidence::where("user_id", auth()->user()["id"])->with(["category", "user"])
            ->get()
            ->map(function ($item) {
                $mediaPath =  pathinfo($item->getFirstMediaPath("Evidences"), PATHINFO_DIRNAME);
                $this->getMediaUrl($item, $mediaPath);
                return $item;
            });
        return $this->createJsonGenericResponse(["evidences" => $evidences]);
    }

    public function postUpdateEvidence(Evidence $evidence, UpdateteEvidenceRequest $request)
    {
        if ($request["category"]) {
            $category = Category::create(["category" => $request["category"]]);
            $request->merge([
                'categoryid' => $category['id'],
            ]);
        }
        $data = [];
        if ($request->categoryid) {
            $data["category_id"] = $request->categoryid;
        }
        if ($request->evidenced) {
            $data["evidence_date"] = $request->evidenced;
        }
        if ($request->description) {
            $data["description"] = $request->description;
        }
        $evidence->update($data);
        if ($request->file('image')) {
            $evidence->clearMediaCollection('Evidences');
            $evidence->addMediaFromRequest('image')
                ->withResponsiveImages()
                ->toMediaCollection("Evidences", 's3');
        }
        return $this->createJsonGenericResponse(["user" => $evidence]);
    }

    private function getMediaUrl(&$item, $path)
    {
        $disk = Storage::disk('s3');
        $media = $disk->allFiles($path);
        if (count($media)) {
            $responsive = [];
            for ($i = 0; $i < count($media); $i++) {
                if ($i === 0) {
                    $item["fullUrl"] =
                        $disk->temporaryUrl(
                            $media[$i],
                            now()->addMinutes(120)
                        );
                } else {
                    $responsive[]
                        = $disk->temporaryUrl(
                            $media[$i],
                            now()->addMinutes(120)
                        );
                }
            }
            $item["responsive"] = $responsive;
        }
    }
}
