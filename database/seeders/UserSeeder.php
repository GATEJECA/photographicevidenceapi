<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{

    public function run()
    {
        User::insert([
            [
                'name' => 'Carlos Garcia',
                'nickname' => 'GATEJECA',
                'type' => 'admin',
                'password' => Hash::make('SuperP@ssword123'),
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Gendra User',
                'nickname' => 'UserTest',
                'type' => 'admin',
                'password' => Hash::make('SuperP@ssword567'),
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }
}
